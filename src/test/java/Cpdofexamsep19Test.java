package com.agiletestingalliance; 
import static org.junit.Assert.*;
import java.io.*;
import org.junit.Test;

public class Cpdofexamsep19Test {

    @Test
    public void testMinMaxCompare() throws Exception {

        int k= new MinMax().minMaxCompare(15,5);
        assertEquals("Compare",k,15);

    }


@Test
    public void testDur() throws Exception {

        String k= new Duration().dur();
        assertEquals("Duration",k.contains("professionals"),true);

    }

@Test
    public void testDesc() throws Exception {

       String k= new Usefulness().desc();
        assertEquals("Usefulness",k.contains("transformation"),true);

    }

@Test
    public void testDescAbout() throws Exception {
String k= new AboutCPDOF().desc();
        assertEquals("AboutCPDOF",k.contains("certification"),true);

    }

	


}

